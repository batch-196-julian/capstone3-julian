const express = require("express");
const router = express.Router();

const auth = require("../auth")
const {verify, verifyAdmin} = auth;

const productControllers = require("../controllers/productControllers");

router.post('/',verify,verifyAdmin,productControllers.addProduct);

router.get('/getAllActiveProduct',productControllers.getAllActiveProduct);

router.get('/getSingleProduct/:productId',productControllers.getSingleProduct)

router.put('/updateProduct/:productId',verify,verifyAdmin,productControllers.updateProduct);

router.patch('/archiveProduct/:productId',verify,verifyAdmin,productControllers.archiveProduct);

router.put('/activateProduct/:productId',verify,verifyAdmin,productControllers.activateProduct);

module.exports = router;


