const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({

	userId: {
		type: String
	},

	receiver: {
		type: String,
		default: null
	},
	
	contactNumber: {
		type: Number,
		default: null
	},

	shippingAddress: {
		type: String,
		default: null
	},

	totalAmount: {
		type: Number,
		required: [true,"Total amount is required"]
	},

	purchaseOn: {
		type: Date,
		default: new Date()
	},

	isCompleted: {
		type: Boolean,
		default: false
	},
	products:[{
				productId: {
					type: String,
					required: [true, "Product is required"]
							},
				quantity: {
					type: Number,
					required: [true, "Quantity is required"]
							}		
		}]
			
	})

module.exports = mongoose.model("Order",orderSchema);
