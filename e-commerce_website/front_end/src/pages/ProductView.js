import { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import Swal from 'sweetalert2';
import UserContext from "../UserContext";

export default function ProductView(){



	const { user } = useContext(UserContext)

	const navigate = useNavigate()

	const { productId } = useParams()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [imageURL, setImageURL] = useState('');
	const [quantity, setQuantity] = useState(1);



	const cart = (product) => {

		console.log(product)
		fetch(`http://localhost:4000/orders`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")
				}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if (data === true) {
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Add to cart successfully"
				})
				navigate("/products")
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}


	function addQuantity(){
		if (quantity >= stocks) {
			Swal.fire({
					title: "Out of Stocks",
					icon: "error",
					text: "Our Stock is limited only!"
				})
		}
		else {
			setQuantity(quantity+1)
		}
		
	}
	function subtractQuantity(){
		if (quantity <= 1) {
			Swal.fire({
					title: "Out of Stocks",
					icon: "error",
					text: "Our Stock is limited only!"
				})
		}
		else {
			setQuantity(quantity-1)
		}
	}

	useEffect(() =>{

		fetch(`http://localhost:4000/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setStocks(data.stocks);
			setPrice(data.price);
			setImageURL(data.imageURL);
		})
	}, [productId])

	return(
		<Container className="mt-5">
			<Row>
				<Col className="col-md-6">
						<Card.Img variant="top" src={`https://drive.google.com/uc?export=view&id=${imageURL}`}  />
				</Col>
				<Col className="col-md-6">
					<Card.Body className="productCatalog">
		                <div className="text">
		                    <h1>{name}</h1>
		                    <div className="price mt-4">
		                        <h4 className="text-danger">&#8369;{price}</h4>
		                        {
		                            stocks > 0
		                            ? <h6 className="text-danger">In Stock: {stocks}</h6>
		                            : <h6 className="text-danger">Out Stock</h6>
		                        }
		                    </div>
		                    <div className="description mb-5">
		                        <p>{description}</p>		                    
		                    </div>

							<div className=" mb-3">

								<button className="btn btn-info mx-2" onClick={() => subtractQuantity()}>-</button>
									<input className="text-center"
						                placeholder="Quantity" 
						                value={quantity}
						                onChange={e => setQuantity(e.target.value)}
						                disabled
						                required
					                />
		            			<button className="btn btn-info ms-2" onClick={() => addQuantity()}>+</button>

							</div>

							<div className="d-grid gap-2">
							{ 
								(user.id !== null)
								?
									(stocks !== 0)
									?
										<Button variant="primary" size="lg" onClick={() => cart()}>Add to Cart</Button>
										<Button className="btn btn-danger"><Nav.Link as={Link} to="/products" eventKey="/products">Back</Nav.Link></Button>
									:
										<Button variant="danger" size="lg">Out of Stock</Button>
										<Button className="btn btn-danger"><Nav.Link as={Link} to="/products" eventKey="/products">Back</Nav.Link></Button>
								:
								<Button as={Link} to="/login" variant="primary" size="lg">Please Login to Order</Button>
							}
							</div>
		                </div>
				    </Card.Body>
				</Col>
			</Row>
		</Container>
	)
}
