import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {


  const {user, setUser} = useContext(UserContext);


  // State hooks to store the values of the input fields
  const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

        function authenticate(e) {

          // Prevents page redirection via form submission
          e.preventDefault();

            // Syntax:
                // fetch('url', options)
            fetch('http://localhost:4000/users/login', {
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if(typeof data.accessToken !== "undefined"){
                    localStorage.setItem('token', data.accessToken);
                    retrieveUserDetails(data.accessToken);

                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "Welcome to Julian Apparel!"
                    });

                } else {

                    Swal.fire({
                        title: "Authentication Failed",
                        icon: "error",
                        text: "Check your credentials"
                    });

                };
            });

          setEmail('');
          setPassword('');


      };

        const retrieveUserDetails = (token) => {

            fetch('http://localhost:4000/users/getUserDetails', {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            })
        }; 

    useEffect(() => {

      if(email !== '' && password !== ''){
          setIsActive(true);
      }else{
          setIsActive(false);
      }

  }, [email, password]);

    return (
        (user.id !== null) ?
            <Navigate to="/products"/>
        :
      <>

        <Form onSubmit={e => authenticate(e)}>
        <h1>Login Here:</h1>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    required
                    value={email}
                onChange={(e) => setEmail(e.target.value)}

                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Enter password" 
                    required
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                />
            </Form.Group>

            <p>Not yet registered? <Link to="/register">Click here</Link> to register.</p>
             <div className="d-grid gap-2 mt-3">
            { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-5">
                    Login
                </Button>
                : 
                <Button variant="danger"  type="submit" id="submitBtn" disabled className="mt-3 mb-5">
                    Login
                </Button>
            }
            </div>
        </Form>
        </>
    )
}
// <Button variant="secondary" size="lg">