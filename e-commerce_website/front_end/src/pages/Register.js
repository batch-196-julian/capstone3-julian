import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const {user} = useContext(UserContext);
	const history = useNavigate();


	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);

	function registerUser(e){
		 // prevents page redirection via form submission
		 e.preventDefault();

		 fetch('http://localhost:4000/users/checkEmailExists', {
		 	method: 'POST',
		 	headers: {
		 		'Content-Type' : 'application/json'
		 	},
		 	body: JSON.stringify({
		 		email:email
		 	})
		 })
		 .then(res => res.json())
		 .then(data => {


		 	if(data){
		 		Swal.fire({
		 			title: "Duplicate email found",
		 			icon: "info",
		 			text: "The email that you're trying to register already exist"
		 		});

		 	} else {
		 		
		 		fetch('http://localhost:4000/users', {
		 			method: 'POST',
		 			headers: {
		 				'Content-Type' : 'application/json'
		 			},
		 			body: JSON.stringify({
		 				firstName: firstName,
		 				lastName: lastName,
		 				email: email,
		 				mobileNo: mobileNo,
		 				password: password
		 			})
		 		})
		 		.then(res => res.json())
		 		.then(data => {


		 			if(data.email){
		 				Swal.fire({
		 					title: 'Registration successful!',
		 					icon: 'success',
		 					text: 'Thank you for registering'
		 				});
		 				
		 				history("/login");

		 			} else {
		 				Swal.fire({
		 					title: 'Registration failed',
		 					icon: 'error',
		 					text: 'Something went wrong, try again'
		 				});
		 			};
		 		});
		 	};
		 });

		 // clear input fields
		 setEmail('');
		 setPassword('');
		 setFirstName('');
		 setLastName('');
		 setMobileNo('');


	};



	useEffect(() => {
		// validation to enable the submit button when all fields are populated and both passwords match
		if(email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, firstName, lastName, mobileNo, password]);

	

	return(
		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>
		<h1>Register Here:</h1>
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter First Name here"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Last Name here"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter 11-digit mobile number here"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					required
					value= {email}
					onChange= {e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>
		<div className="d-grid gap-2 mt-3">
            { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-5">
                    Register
                </Button>
                : 
                <Button variant="danger"  type="submit" id="submitBtn" disabled className="mt-3 mb-5">
                   Register
                </Button>
            }
            </div>
		</Form>
		</>

	)
}

