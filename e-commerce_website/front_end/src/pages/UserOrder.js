// import { Table, Button, Container, Navbar, NavbarBrand, Nav, NavItem } from 'react-bootstrap';
// import { useState, useEffect, useContext } from "react";
// import UserContext from "../UserContext"
// import { Navigate, useParams, Link } from "react-router-dom";

// export default function UserOrder(){

//   	// useContect
// 	const { user } = useContext(UserContext)

//     const { orderId } = useParams()

// 	// useState
// 	const [allProducts, setAllProducts] = useState([]);

// 	// "fetchData()" wherein we can invoke if their is a certain change with the course
	
// 	const fetchData = () => {
// 		fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}`,
// 		{
// 			headers: {
// 				'Authorization': `Bearer ${localStorage.getItem("token")}`
// 				}
// 		})
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data)
// 			setAllProducts(data.order.map(userOrders => {
// 				console.log(userOrders)
// 				return(
// 					<tr key={userOrders._id}>

// 						    <td>{userOrders.id}</td>
// 						    <td>{userOrders.name}</td>
// 						    <td>{userOrders.price}</td>
// 						    <td>{userOrders.quantity}</td>
// 						    <td>{userOrders.itemTotal}</td>
						    
// 					</tr>
// 				)
// 			}))
// 		})
// 	}

// 	useEffect(() => {
// 		// fetchData();
// 	}, [])

// 	return(

// 		(user.isAdmin)
// 		?
// 		<>
// 			<Container className="pt-3">
// 				<Navbar >
// 					<Container>
// 						<NavbarBrand><h1 className="header-admin">Order Dashboard</h1></NavbarBrand>
// 						<Nav>
// 							<NavItem>
// 								<Button variant="primary" size="sm" as={Link} to={`/admin`} className="mx-2">Products</Button>
// 								<Button variant="success" size="sm" as={Link} to={`/order`} className="mx-2">Orders</Button>
// 							</NavItem>
// 						</Nav>
						
// 					</Container>
// 				</Navbar>
// 				{/*Table Start*/}
// 				<Table striped bordered hover>
// 				    <thead>
// 				        <tr>
// 				          	<th>Product Id</th>
// 				          	<th>Product Name</th>
// 				          	<th>Price</th>
// 				          	<th>Quantity</th>
// 				          	<th>Total</th>
// 				        </tr>
// 				    </thead>
// 				    <tbody>
// 				        { allProducts }
// 				    </tbody>
// 				</Table>
// 				{/*Table End*/}
// 			</Container>

// 		</>
// 		:
// 		<Navigate to="/products" />
// 	)
// }
