import { useState, useContext } from "react";
import React from "react";
import {useCart} from "react-use-cart";
import { Modal, Button, Form} from "react-bootstrap";
import Swal from "sweetalert2"
import {Link} from "react-router-dom";
import UserContext from "../UserContext";

import "../style/cart.css"

const Cart = () =>{


	const { user } = useContext(UserContext)

	// Modal
	const [ show, setShow ] = useState(false);
  	const modalClose = () => setShow(false);
  	const modalOpen = () => setShow(true);


	const [receiver, setReceiver] = useState("");
	const [contactNumber, setContactNumber] = useState("");
	const [address, setAddress] = useState("");


	const { isEmpty, totalUniqueItems, items, totalItems, cartTotal, updateItemQuantity, removeItem, emptyCart } = useCart();


	if(isEmpty) return <h1 className="p-5 text-center justify-content-md-center header-title">Your Cart is Empty</h1>

	const  checkout = (e) => {

		e.preventDefault();
			fetch(`http://localhost:4000/orders`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${localStorage.getItem("token")
					}`
				},
				body: JSON.stringify({
					userId: user.id,
					receiver: receiver,
			        contactNumber: contactNumber,
			        shippingAddress: address,
			        totalAmount: cartTotal,
					item: items
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if (data === true) {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again"
					})
					
				}
				else{
					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Add to cart successfully"
					})
					emptyCart()
				}
			})

	}

	function emptyMyCart(myProduct){
			Swal.fire({
					title: "Success",
					icon: "success",
					text: "You have successfully empty your cart!"
				})
			emptyCart()
		}

	return(
		<section className="py-4 container">
			<div className="row justify-content-center">
				<div className="col-12 table-responsive my-3">
					<h5>Shopping Cart ({totalUniqueItems}) total Items: ({totalItems})</h5>
					<table className="table my-5 align-middle">
					<thead>
				        <tr>
				          	<th className="w-50">Product Name</th>
				          	<th className="w-25">Price</th>
				          	<th colSpan="3" className="text-center">Quantity</th>
				          	<th className= "text-center">Action</th>
				        </tr>
				    </thead>
					<tbody>
						{
							items.map((item, index)=>{
								return(
									<tr key={index}>
										<td >
											<Link as={Link} to={`/products/${item.id}`} className="text-bold" eventKey="products">{item.name}</Link>
										</td>
										<td className="text-bold">&#8369; {item.price}</td>
										<td>
											<button className="btn btn-warning ms-4" onClick={()=> updateItemQuantity(item.id, item.quantity - 1)}>-</button>
										</td>
										<td>
											<input className="text-center" placeholder="Quantity" value={item.quantity} disabled required />
										</td>
										<td>
											<button className="btn btn-secondary ms-2" onClick={()=> updateItemQuantity(item.id, item.quantity + 1)}>+</button>
										</td>
										<td>
											<button className="btn btn-danger ms-4" onClick={()=>removeItem(item.id)}>Remove</button>
										</td>
										
									</tr>
								)
							})
						}

					</tbody>
					</table>
				</div>
				<div className="col-auto ms-auto">
					<h2>Total Amount: &#8369;{cartTotal}</h2>
				</div>
				<div className="col-auto">
					<button className="btn btn-danger m-2" onClick={()=> emptyMyCart()}>Clear Cart</button>
					<button className="btn btn-primary m-2" onClick={modalOpen} >Checkout</button>
				</div>
			</div>

			<Modal show={show} onHide={modalClose}>
		        <Form onSubmit ={(e) => checkout(e)}>
			        <Modal.Header closeButton>
			          	<Modal.Title>Delivery Information</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			            <Form.Group className="mb-3" controlId="receiver">
			              	<Form.Label>Receiver Name </Form.Label>
			              	<Form.Control type="text" placeholder="Input receiver name" onChange={e => setReceiver(e.target.value)} autoFocus />
			            </Form.Group>
			            <Form.Group className="mb-3" controlId="contactNumber">
			              	<Form.Label>Contact Number</Form.Label>
			              	<Form.Control type="number" onChange={e => setContactNumber(e.target.value)} placeholder="Input contact"  />
			            </Form.Group>
			            <Form.Group className="mb-3" controlId="address">
			              	<Form.Label>Shipping Address</Form.Label>
			              	<Form.Control as="textarea" rows={4} onChange={e => setAddress(e.target.value)} placeholder="Input shipping address"/>
			            </Form.Group>
			        </Modal.Body>
			        <Modal.Footer>
			          	<Button variant="secondary" onClick={modalClose}>
			            	Close
			          	</Button>
			          	<Button variant="primary" type="submit" id="submitBtn">
			            	Save
			          	</Button>
			        </Modal.Footer>
		        </Form>
	      	</Modal>
		</section>

		)
}
export default Cart;
