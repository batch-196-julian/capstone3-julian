import { Link } from "react-router-dom";  
import {Navbar, Nav, Container,Button  } from "react-bootstrap";
import {useContext} from "react";


import UserContext from "../UserContext"


function AppNavbar() {

    const {user} = useContext(UserContext);

  return (
    <Navbar collapseOnSelect expand="lg" bg="white" variant="white">
      <Container className="Navbar ">
       <Navbar.Brand as={Link} to="/" eventKey="/">
            <img
              alt=""
              src="./images/logo.jpg"
              width="150"
              height="70"
              className="d-inline-block align-top"/>
          </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mx-auto">
            <Nav.Link  className="ms-3" as={Link} to="/" eventKey="/">Home</Nav.Link>
             {
                (user.isAdmin)
                ? <>
                  <Nav.Link className="ms-3" as={Link} to="/products" eventKey="/products">Products</Nav.Link>
                  <Nav.Link className="ms-3" as={Link} to="/admin" eventKey="/admin">Admin Dashboard</Nav.Link>
                  </>
                :
                  <>  
                    <Nav.Link className="ms-3" as={Link} to="/products" eventKey="/products">Products</Nav.Link>
                    <Nav.Link className="ms-3" as={Link} to="/cart" eventKey="/cart">Cart</Nav.Link>
                  </>


              }
          </Nav>
          <Nav>
            { 
                (user.id !== null)
                ? <>
                  

                  <Nav.Link as={Link} to="/logout" eventKey="/logout"><Button variant="outline-danger">Logout</Button></Nav.Link>
                  </>   
                :
                  <>

                  <Nav.Link eventKey={2} as={Link} to="/login" eventKey="/login"><Button variant="outline-success">Login</Button></Nav.Link>
            <Nav.Link eventKey={2} as={Link} to="/register" eventKey="/login"><Button variant="outline-primary">Register</Button></Nav.Link>
                  </>
              }

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AppNavbar;