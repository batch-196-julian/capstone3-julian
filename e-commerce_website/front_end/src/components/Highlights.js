import {Row, Col, Card} from 'react-bootstrap';


export default function Highlights(){
	return(

			<Row xs={1} md={3} className="g-4 pb-5">

	        <Col>
	          <Card className="cardHighlight">
	            <Card.Img variant="top" src="./images/high_quality.jpg" />
	            <Card.Body>
	              <Card.Title><b>High Quality</b></Card.Title><br/>
	              <Card.Text>
	               Natural fibers such as cotton, wool, silk, linen, and cashmere are generally a better bet when choosing clothes that will last. While synthetic fibres can bring levels of stability and versatility to garments, as a rule of thumb don't buy a garment with more than a 20% portion of synthetic fiber.
	              </Card.Text>
	            </Card.Body>
	          </Card>
	        </Col>

	        <Col>
	          <Card className="cardHighlight">
	            <Card.Img variant="top" src="./images/fast_shipping.jpg" />
	            <Card.Body>
	              <Card.Title><b>Fast Shipping</b></Card.Title><br/>
	              <Card.Text>
	                On-demand pick-up and delivery services your business can rely on. Get special quote today. Multi drop-off points. AI-optimized routes. Real-time tracking & photo-proof of delivery. Free Package Protection. Designated Drivers. AI Route Optimization.
	              </Card.Text>
	            </Card.Body>
	          </Card>
	        </Col>

	        <Col>
	          <Card className="cardHighlight">
	            <Card.Img variant="top" src="./images/affordable_price.jpg" />
	            <Card.Body>
	              <Card.Title><b>Affordable Prices</b></Card.Title><br/>
	              <Card.Text> We want a good quality railway that is widely available at an affordable price. If other manufacturers wanted to compete by charging less than a particular price, we could ensure a supply of products—including branded products—at an affordable price.
	              </Card.Text>
	            </Card.Body>
	          </Card>
	        </Col>   
	    </Row>
	  );
	}