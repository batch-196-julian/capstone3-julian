import React from "react";
import { Link } from "react-router-dom";
import { Card, Button, Col} from "react-bootstrap";
import { useCart } from "react-use-cart";
import Swal from "sweetalert2"

import { useState, useEffect } from "react"

import '../style/product.css';

const ProductCard = (props) => {

	const { addItem } = useCart()
	const [myProduct, setMyProduct] = useState()


	function addToCart(myProduct){
			Swal.fire({
					title: "Success",
					icon: "success",
					text: "Add to cart successfully!"
				})
			addItem(myProduct)
		}

	return (
		<>
				<Col className="col-12 col-md-4 col-lg-3 mb-5">
			    	<Card className="cardHighlight">
			    		<Card.Img variant="top" src={`https://drive.google.com/uc?export=view&id=${props.product.imageURL}`}  />
				        <Card.Body className="productCatalog">
			                <div className="text">
			                    <h5></h5>
			                    <h3>{props.title}</h3>
			                    <div className="price">
			                        <h5 className="text-danger">&#8369;{props.price}</h5>
			                        {
			                            props.product.stocks > 0
			                            ? <h5 className="text-danger">Stocks: {props.product.stocks}</h5>
			                            : <h5 className="text-danger">Out Stock</h5>
			                        }
			                    </div>
			                    <div className="description">
			                        <h5>Description</h5>
			                        <p>{props.description}</p>
			                    </div>
			                </div>
				        </Card.Body>
				        <div className="d-grid gap-2">
				        	    
				       <Button as={Link} to={`/products/${props.product._id}`} variant="primary">View</Button>

						</div>		
				      </Card>
					</Col>
		</>
		
				);
			}
export default ProductCard
